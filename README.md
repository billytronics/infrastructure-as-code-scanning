# Infrastructure As Code Scanning

> **Blog post: [Fantastic Infrastructure as Code security attacks and how to find them](https://about.gitlab.com/blog/2022/02/17/fantastic-infrastructure-as-code-security-attacks-and-how-to-find-them/)**. 

Demo project for [Infrastructure as Code Scanning](https://docs.gitlab.com/ee/user/application_security/iac_scanning/) introduced in [GitLab 14.5](https://about.gitlab.com/releases/2021/11/22/gitlab-14-5-released/#introducing-infrastructure-as-code-iac-security-scanning).

- [Dockerfile](Dockerfile) uses bad practices [detected by kics queries](https://docs.kics.io/latest/queries/dockerfile-queries/)
- [terraform/](terraform/) provides Terraform examples 
   - [terraform/aws](terraform/aws) creates an AWS S3 bucket which is world readable and [detected by queries](https://docs.kics.io/latest/queries/terraform-queries/)
   - [terraform/module](terraform/module) imports a vulnerable AWS S3 module from https://gitlab.com/gitlab-de/use-cases/iac-tf-vuln-module 
- [kubernetes/](kubernetes/) creates a deployment and service with missing best practices for IaC efficiency    

[integrations/](integrations/) provides a script which parses a kics report, creates a Markdown table, and sends a comment into the merge request related to the branch. This needs `GITLAB_TOKEN` as CI/CD variable, not protected. Use a project/group access token with `api` permissions. 

## Instructions

- Follow the blog post (TODO) to run the scanners
- Fork/copy the project
  - CI/CD > Pipelines > Run manually
- Inspect the security reports
  - As MR widget: https://gitlab.com/gitlab-de/playground/infrastructure-as-code-scanning/-/merge_requests/1 
  - Navigate to Security & Compliance > Vulnerability Report


## Screenshots

![Vulnerability reports](docs/images/iac_scans_vuln_reports.png)
![Detail for Terraform AWS S3](docs/images/iac_scans_vuln_details_terraform_aws_s3.png)
![Code highlight for Terraform AWS S3](docs/images/iac_scans_vuln_details_terraform_aws_s3_code.png)
![Code highlight for Docker](docs/images/iac_scans_vuln_details_docker_code.png)

